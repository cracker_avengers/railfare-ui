echo "set \$gateway http://${BACKEND_URL};" > /etc/nginx/envvars.conf
echo resolver `grep nameserver /etc/resolv.conf | head -n1 | awk '{print $2}'` ";" > /etc/nginx/resolvers.conf
