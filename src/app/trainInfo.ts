export class TrainInfo {

  constructor(
    public departure_station: string,
    public arrival_station: string,
    public time_of_departure: string
  ) { }

}
