import {Component, OnDestroy, OnInit} from '@angular/core';
import {Station} from '../model/station.model';
import {ActivatedRoute} from '@angular/router';
import {HttpService} from '../service/http.service';
import {Restaurant} from '../model/restaurant.model';

@Component({
  selector: 'app-restaurants',
  templateUrl: './restaurants.component.html',
  styleUrls: ['./restaurants.component.css']
})
export class RestaurantsComponent implements OnInit, OnDestroy {
  id: number;
  private sub: any;
  restaurants: Restaurant[] = [];
  rest_id = 1;

  constructor(private route: ActivatedRoute, private httpServ: HttpService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number

      // In a real app: dispatch action to load the details here.
    });
    this.httpServ.getRestaurantsByStationId(this.id).subscribe(data => this.restaurants = data['restaurants']);

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
