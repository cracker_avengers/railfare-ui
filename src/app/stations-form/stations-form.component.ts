import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Station} from '../model/station.model';
import {HttpService} from '../service/http.service';

@Component({
  selector: 'app-stations-form',
  templateUrl: './stations-form.component.html',
  styleUrls: ['./stations-form.component.css']
})
export class StationsFormComponent implements OnInit, OnDestroy {
  id: string;
  private sub: any;
  stations: Station[] = [];

  station_id = 1;

  constructor(private route: ActivatedRoute, private httpServ: HttpService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id']; // (+) converts string 'id' to a number

      // In a real app: dispatch action to load the details here.
    });
    this.httpServ.getStationsById(this.id).subscribe(data => this.stations =
        data['stops'].map(
      el => {
        let s = new Station();
        s.code = el.station.code;
        s.name = el.station.name;
        return s;
      }));
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
