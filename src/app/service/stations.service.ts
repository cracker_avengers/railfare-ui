import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Station} from '../model/station.model';


@Injectable()
export class UserService {
  constructor(private http: HttpClient) { }
  // baseUrl = 'http://api-route-railfare.7e14.starter-us-west-2.openshiftapps.com';

  getStationsByName(name: string) {
    return this.http.get<Station[]>('/api/stations/?name=' + name);
  }
}
