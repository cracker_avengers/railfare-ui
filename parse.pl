#!/usr/bin/perl

use MIME::Base64;


my $secret;
if($ARGV[0]=~m/\.tmp\.secret$/) {
    $secret = 1;
}
elsif($ARGV[0]=~m/\.tmp$/) {
    $secret = 2;
}
while(<>){
    $_ =~ m/\{\{\s*\$(\S+?)\s*\}\}/;
    my $code;
    if(defined($secret) && $1){
        $code = $ENV{$1} or die "$1 not set";
        $code = encode_base64($code) if $secret == 1 ;
        s/\{\{\s*\$(\S+?)\s*\}\}/$code/ge;
    }
    print "$_";
}