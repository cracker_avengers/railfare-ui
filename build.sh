#!/bin/bash

VERSION="0.1";
NAME="natalyasann/railfare-ui";
TAG="$VERSION.$CIRCLE_BUILD_NUM";
STABLE="stable";
[[ "dev" = "$CIRCLE_BRANCH" ]] && TAG="dev-$TAG";
IMG="$NAME:$TAG"
echo "TAG : ${TAG}"
docker build -t "$IMG" .
[[ "master" = "$CIRCLE_BRANCH" ]] && docker tag "$IMG" "$NAME:$STABLE";
BUILD_STAT="$?"
if [[ "dev" = "$CIRCLE_BRANCH" ]] ; then
    echo "$IMG";
    docker login -u "$DOCKER_LOGIN" -p "$DOCKER_PASSWORD" && docker push "$IMG"
    elif [[ "master" = "$CIRCLE_BRANCH" ]] ; then
    echo "$NAME:$STABLE"
    docker login -u "$DOCKER_LOGIN" -p "$DOCKER_PASSWORD" && docker push "$NAME:$STABLE"
else echo "Build status: $BUILD_STAT"
fi
