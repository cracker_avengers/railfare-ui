import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// library for forms
import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';

import { map } from 'rxjs/operators';

// library for api
import { HttpClientModule } from '@angular/common/http';

import { TrainFormComponent } from './train-form/train-form.component';
import {HttpService} from './service/http.service';
import { StationsFormComponent } from './stations-form/stations-form.component';
import {RouterModule, Routes} from '@angular/router';
import { RestaurantsComponent } from './restaurants/restaurants.component';
import { MenuComponent } from './menu/menu.component';
/*
const routes = [
  {path: '', component: TrainFormComponent},
  {path: 'stations', component: StationsFormComponent}
];*/

export const routes: Routes = [
  {path: '', component: TrainFormComponent},
  {path: 'stations/:id', component: StationsFormComponent },
  {path: 'restaurants/:id', component: RestaurantsComponent},
  {path: 'menu/:id', component: MenuComponent}

];

@NgModule({
  declarations: [
    AppComponent,
    TrainFormComponent,
    StationsFormComponent,
    RestaurantsComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [HttpService],
  bootstrap: [AppComponent]
})
export class AppModule { }
