FROM cecton/nginx-with-substitution-filter

ENV APP_ROOT=/opt/app-root
ENV PATH=${APP_ROOT}/bin:${PATH}

EXPOSE 8080
COPY nginx.conf              /etc/nginx/nginx.conf
COPY default.conf            /etc/nginx/conf.d/default.conf
COPY resolver.sh             /tmp/resolver.sh
COPY /dist/railfare-ui       /usr/share/nginx/html
COPY uid.sh                  ${APP_ROOT}/bin

RUN chmod -R 777 ${APP_ROOT}/bin /var/log/nginx /var/cache/nginx/ /etc/nginx/ /var/run/ /usr/share/nginx/html/ \
    && chmod +x /tmp/resolver.sh

CMD ["uid.sh"]

USER 10001

CMD sh /tmp/resolver.sh && nginx -g "daemon off;"
