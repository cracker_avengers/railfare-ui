import {Component, OnInit} from '@angular/core';

import {TrainInfo} from '../trainInfo';

// library for api
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import {Station} from '../model/station.model';
import {HttpService} from '../service/http.service';
import {Train} from '../model/train.model';


@Component({
  selector: 'app-train-form',
  templateUrl: './train-form.component.html',
  styleUrls: ['./train-form.component.css'],
  providers: [HttpService]
})
export class TrainFormComponent implements OnInit {
  stations: Station[];
  records = {};
  trains: Train[] = [];
  constructor(private httpServ: HttpService) {}

  // Base model to show
  model = new TrainInfo('s9602494', 's9623135', '2018-12-28');

  submitted = false;
  chosen = false;

  // put list of trains into this.trains
  onSubmit() {
    this.submitted = true;
    this.httpServ.getTrains(this.model.departure_station, this.model.arrival_station, this.model.time_of_departure).subscribe(
      (data: Train[]) => this.trains = data);
  }

  // clean the model
  newTrainInfo() {
    this.model = new TrainInfo('Saint-Petersburg', '', '');
  }

  // choose the specific train
  chooseTrain(train: Train) {
    this.chosen = true;
    console.log(train);
  }
/*
  goToStations(id) {
    this.router.navigate(['/product-details', id]);
  }*/
  // TODO: Remove this when we're done
  get diagnostic() {
    return JSON.stringify(this.model);
  }

  ngOnInit() {}

    // this.httpServ.getData().subscribe((data: Station[]) => this.stations = data);
    /* new - works
    this.records = this.httpServ.getData().subscribe(data => {
      console.log('We got', data.obj);
    });*/
    /* new2 - works for 1 train
    this.httpServ.getData().subscribe((data: Train) => this.train = data);*/


    // this.httpServ.getData('Moscow').subscribe((result) => console.log(result));

    /*const observable = this.http.get('/stations/?name=Москва');

    observable.subscribe((result) => console.log(result));*/
    /*this.http.get('/stations/?name=Москва')
      .map(res => res.json());*/
    /*   this.http.get('/stations/?name=Москва')
      .pipe(map(data => {})).subscribe(result => {
      console.log(result);
    });*/
}
