import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Station} from '../model/station.model';
import {environment} from '../../environments/environment';

interface Data {
  obj: any;
}

@Injectable()
export class HttpService {
  constructor(private http: HttpClient) { }
  getTrains(dep: string, arr: string, date: string ) {
    return this.http.get(environment.prefix + `/trains/search?arr=${arr}&dep=${dep}&date=${date}`);
  }
  getStationsById(id: string) {
    return this.http.get(environment.prefix + `/trains/${id}`);
  }

  getRestaurantsByStationId(id: number) {
    return this.http.get('/assets/rest_for_station' + id + '.json');
  }

}
